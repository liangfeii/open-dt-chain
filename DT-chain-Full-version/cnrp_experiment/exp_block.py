# 模拟 block 其他结构
import time

from cnrp_experiment.exp_cnrp import TestCNRP
from cnrp_experiment.exp_config import BLOCK_TX_NUM


class TestBlockHeader(object):
    def __init__(self, height: int):
        # block id <int>
        self.height = height
        # dict
        self.cnrp = TestCNRP()


class TestTransaction(object):
    def __init__(self, tx_id: int, tx_type: str, source_node_id: int):
        # int
        self.tx_id = tx_id
        # str
        self.tx_type = tx_type
        # int
        self.source_node_id = source_node_id
        # int
        self.prev_tx_id = -1

        # float
        self.data_score = 0.7
        # float
        self.data_quality = 0.7

        # str
        self.data_id = -1
        # str
        self.business_id = -1

    # 发布 upload tx 时调用
    def set_data_id_upload(self):
        if self.tx_type == 'upload':
            self.data_id = str(time.time()) + '-' + str(self.source_node_id)
        else:
            return


class TestBlock(object):
    def __init__(self, block_header: TestBlockHeader):
        self.block_header = block_header

        self.transactions = []

        self.tx_num = 0

    def add_tx(self, tx: TestTransaction):
        if self.tx_num < BLOCK_TX_NUM:
            self.transactions.append(tx)
            self.tx_num += 1
        else:
            return

    @property
    def is_full(self):
        return self.tx_num >= BLOCK_TX_NUM






