

class TestCNRP_BlockInfo(object):
    # 本块内的量 计算的间接依据
    def __init__(self):
        # B_num: {[node id <int>]: [发布有效类型tx数目 <int>], ...}
        self.B_num = {}
        # P_N_num: [被评价的目标地址]: {[评价的目标地址]: [积极评价数目,消极评价数目 <str>]}
        # {[被评价的目标节点 target node id <int>]: {[评价的来源地址 source_node_id <int>]: [该节点的正负评价记录 <list>], ...},
        #   ...
        # }
        self.P_N_num = {}


class TestCNRP_ComputeTemp(object):
    # 累积量 注：累积量是计算的直接依据
    def __init__(self):
        # B_num: {[node id <int>]: [发布有效类型tx数目 <int>], ...}
        self.B_num = {}
        # P_N_num: [被评价的目标地址]: {[评价的目标地址]: [积极评价数目,消极评价数目 <str>]}
        # {[被评价的目标节点 target node id <int>]: {
        #       [评价的来源地址 source_node_id <int>]: [该节点的正负评价记录, 节点分子分母单个因式项 (*4) <list>],
        #       ...},
        #   ...
        # }
        self.P_N_num = {}
        # fraction: {[被评价的目标节点 target node id <int>]: [分子分母暂存值 <list>], ...}
        self.E_fraction = {}
        # B: {[node_id]: [value]}
        self.B = {}
        # E: {[node_id]: [value]}
        self.E = {}


# ---------------------------------------------


class TestCNRP(object):
    def __init__(self):
        # 当前块的数目信息
        self.block_info = TestCNRP_BlockInfo()
        # 计算暂存值
        self.compute_temp = TestCNRP_ComputeTemp()
        # rp 最终值 {[地址]: [最终rp值 <int>], ...}
        self.value = {}


