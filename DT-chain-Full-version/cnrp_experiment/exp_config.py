

class GlobalDict:
    # node dict 全局变量 {[node id]: [node object]}
    # node id: 1 ~ 100
    node_dict = {}

    # chain dict 全局变量 {[height]: [block object]}
    chain_dict = {}

    # chain dict:
    # height: start from 0, height -1 is genesis block.
    # tx id: start from 0, tx id -1 is genesis tx.

    # Every block contain BLOCK_TX_NUM txs, for example: BLOCK_TX_NUM = 100,
    # tx id: 0 ~ 99 in height 0 block, ...as follow list:
    # height 0: tx id 0 ~ 99
    # height 1: tx id 100 ~ 199
    # height 2: tx id 200 ~ 299
    # ...

    last_height = -1
    last_tx_id = -1

    # 当前的block
    cur_block = {}

    # 计时变量
    time_consume = {'RP_total': 0.0, 'RP_block': {}}


# ---- 计算配置项 ---------------------


# 结构参数 suggest: 100
BLOCK_TX_NUM = 100

# 窗口大小
CNRP_WINDOW_LENGTH = 100

# ---- 计算相关值 ---------------------


CNRP_B_positive = 0.008

# b_punish: 惩罚系数
CNRP_b_punish = 1

# R_init: 初始信誉值
CNRP_R_init = 0.5

# 初始 E 值
CNRP_E_init = 0.5

# 打分补足阈值 当打分个数小于该阈值时，自动补齐该阈值个数的虚拟节点的打分记录
CNRP_threshold_rp_num = 4
# 虚拟节点的默认 S_ab 取值 0.5
CNRP_S_virtual_node = 0.5


