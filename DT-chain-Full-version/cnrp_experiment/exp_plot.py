import matplotlib.pyplot as plt
import time


def plot_test():

    length = 10

    data_x = [_/10 for _ in range(length)]
    data_y1 = [(1 - _/12) for _ in range(length)]
    data_y2 = [(1 - _/11) for _ in range(length)]
    data_y3 = [(1 - _/10) for _ in range(length)]

    # 绘图
    # linstyle 为折线段的样式 全部样式为 '-' '--' ':' '-.'
    # marker为折线点的特殊样式 更多样式详情见 https://blog.csdn.net/caoxinjian423/article/details/111477429
    plt.plot(data_x, data_y1, color='red', linewidth=2.0, linestyle='-', label='DT-chain',
             marker='o', markeredgecolor='r', markersize='4', markeredgewidth=4)
    plt.plot(data_x, data_y2, color='blue', linewidth=2.0, linestyle='--', label='Trust-edge-chain')
    plt.plot(data_x, data_y3, color='green', linewidth=2.0, linestyle='-.', label='no-trust-model')

    # 使用 xticks yticks 修改坐标轴刻度
    # plt.xticks(x,[i for i in data.index])

    plt.title("Comparison Chart of Interaction Success Rate")
    plt.xlabel("Proportion of malicious nodes")
    plt.ylabel("Interaction success rate")

    # 图例
    plt.legend()
    # 调整位置的图例
    # plt.legend(loc="upper left")

    # 保存矢量图 保存位置: ./figures
    foo_fig = plt.gcf()
    foo_fig.savefig('figures/figure-' + str(time.time()) + '.eps', format='eps', dpi=1000)

    # 网格标线
    # plt.grid()

    plt.show()


# main:
plot_test()

