import pickle


class NodeFile(object):

    NODE_FILE_NAME = 'local_data/local_node.dat'

    def __init__(self, node_file_name=NODE_FILE_NAME):
        self.node_file_name = node_file_name
        try:
            with open(node_file_name, 'rb') as f:
                self.node = pickle.load(f)
        except IOError:
            self.node = {}

    def __getitem__(self, key):
        return self.node[key]

    def __setitem__(self, key, value):
        self.node[key] = value

    def delete_local_node(self):
        self.node = {}
        self.save()

    def has_local_node(self):
        # print(self.__dict__)
        if self.node == {} or self.node['local_node'] == {}:
            return False
        else:
            return True

    def get(self, key, default=None):
        return self.node.get(key, default)

    def save(self):
        with open(self.node_file_name, 'wb') as f:
            pickle.dump(self.node, f)

    def items(self):
        return self.node.items()


# if __name__ == "__main__":

    # node = Node.generate_node()
    # node_key = NodeKey()
    #
    # node_key['local_node'] = node
    #
    # node_key.save()

    # node_key = NodeKey()
    # print(node_key)
    # print(type(node_key))
    #
    # print(node_key['local_node'])

    # =======================

    # node = Node.generate_node()
    #
    # node_file = NodeFile()
    # node_file['local_node'] = node
    # node_file.save()
    #
    # node_file = NodeFile()
    # node_2 = node_file['local_node']
    #
    # print(node)
    # print(type(node))
    # print(node_2)
    # print(type(node_2))
    #
    # print(node.sensor_list)
    # print(node_2.sensor_list)
    # node_2.sensor_list = [1, 2, 3]
    # node_file['local_node'] = node_2
    # node_file.save()
    #
    # node_file = NodeFile()
    # node_3 = node_file['local_node']
    # print(node_3.sensor_list)






