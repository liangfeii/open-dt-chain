import random
import time


# class: 传感器
# 传感器 设定：拥有较少计算能力，因此不具有数据处理能力
# by liang fei 2021.06.29


# class: 传感器
# Attributes:
#     sensor_hash (str): public key hash
#     data_list (sensor_data object): data class
#     reputation (int):
from sensor_data import SensorData


class Sensor(object):

    DEFAULT_VALUE_REPUTATION = '1'

    def __init__(self, sensor_id='', reputation=DEFAULT_VALUE_REPUTATION):
        self.sensor_id = sensor_id
        self.sensor_public_key = ''  # 公钥
        self.sensor_private_key = ''  # 私钥
        self.reputation = reputation
        self.data_list = []

    # 添加数据
    def add_data(self, sensor_data_object):
        self.data_list.append(sensor_data_object)
        return

    def get_data(self):

        # 以下测试间隔一定时间产生数据
        for _ in range(10):

            data = []
            for n in range(20):

                data.append([[random.randint(-100, 100), random.randint(-100, 100)],
                             [random.randint(-100, 100), random.randint(-100, 100)]
                            ])

            test_data = SensorData(data)

            print(test_data.data)
            print('')
            print(test_data.data_size)
            print('')
            print(test_data.get_data_size())
            print('')
            print(test_data.data_size)
            print('')
            print(test_data.__dict__)

            time.sleep(2)


# test:
if __name__ == "__main__":
    s = Sensor()
    s.get_data()



