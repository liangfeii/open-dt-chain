#
#
#
#
#


class SensorData(object):

    DEFAULT_VALUE_REPUTATION = 1

    # func: 构建函数
    # data (list): 可自定义维度
    def __init__(self, data=[], data_id='', data_time='', data_remarks='', data_key='',
                 reputation=DEFAULT_VALUE_REPUTATION):
        self.data = data  # 数据
        self.data_id = data_id  # 数据id 自定义
        self.data_time = data_time  # 生成时间
        self.data_remarks = data_remarks  # 备注
        self.data_key = data_key  # 对称加密秘钥 真正数据的秘钥 拥有它相当于拥有数据的使用权
        self.data_size = []  # 数据形状
        self.data_reputation = reputation

    def get_data_size(self):
        self.data_size.append(len(self.data))
        _inner = self.data[0]
        while type(_inner) is list:
            self.data_size.append(len(_inner))
            _inner = _inner[0]
        return self.data_size


# test
if __name__ == "__main__":
    d = [
        [[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]],
        [[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]],
    ]

    test_data = SensorData(d)

    print(test_data.data)
    print('')
    print(test_data.data_size)
    print('')
    print(test_data.get_data_size())
    print('')
    print(test_data.data_size)
    print('')
    print(test_data.__dict__)

